CC = gcc
LD = gcc
TARGET = ntpc
CFLAGS = -Wall -Wextra
LIBS = -lrt

NTPC_OBJ = main.o ntp.o

all: $(TARGET)

ntpc: $(NTPC_OBJ)
		$(LD) $(CFLAGS) $^ $(LIBS) -o $@


%.o: %.c
		$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c ntp_proto.h ntp.h
ntp.o: ntp.c ntp.h ntp_proto.h


clean:
		@rm -f $(TARGET) $(NTPC_OBJ)

rebuild: clean $(TARGET)
