#ifndef _NTP_H_INCLUDED_
#define _NTP_H_INCLUDED_

#include <time.h>

/*
 * NTP Info structure
 */
typedef struct {
	struct timespec lbegin; // local begin
	struct timespec lend;   // local end
	struct timespec lavg;   // local average
	struct timespec rbegin; // remote begin (received)
	struct timespec rend;   // remote end (transmitted)
	struct timespec ravg;   // remote average
} ntp_info;


// zjisti cas a naplni strukturu out, timeout v ms pohlida, ze se dockame
int ntp_query_server(const char *name, ntp_info *out, unsigned timeout);

// nenula zapina debug
void ntp_set_verbose_level(int);

// pricte k prvnimu parametru ten druhy
void ntp_add_timespec(struct timespec *res, const struct timespec *what);

// odecte od prvniho parametru ten druhy
void ntp_sub_timespec(struct timespec *res, const struct timespec *what);



#endif
